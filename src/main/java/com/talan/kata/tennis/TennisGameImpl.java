package com.talan.kata.tennis;

/**
 * Created on 02/04/2022
 * <p>This class helps run the test successfully</p>
 * @author manu
 */
public class TennisGameImpl implements TennisGame {
    private final String player1Name;
    private final String player2Name;
    private int player1Score;
    private int player2Score;
    private int scoreDifference;

    public TennisGameImpl(String player1Name, String player2Name) {
        this.player1Name = player1Name;
        this.player2Name = player2Name;
    }

    @Override
    public void wonPoint(String playerName) {
        if (this.player1Name.equals(playerName))
            this.player1Score++;
        else
            this.player2Score++;
    }

    @Override
    public String getScore() {
        setScoreDifference();

        if (isDraw())
            return drawnScore();
        else if (isWin())
            return wonScore();
        else if (isAdvantage())
            return advantagedScore();
        else
            return otherScore();
    }

    private void setScoreDifference() {
        this.scoreDifference = this.player1Score - this.player2Score;
    }

    private boolean isDraw() {
        return this.player1Score == this.player2Score;
    }

    private String drawnScore() {
        switch (this.player1Score) {
            case 0:
                return "Love-All";
            case 1:
                return "Fifteen-All";
            case 2:
                return "Thirty-All";
            default:
                return "Deuce";
        }
    }

    private boolean isWin() {
        return (this.player1Score > 3 || this.player2Score > 3) &&
                (this.scoreDifference >= 2 || this.scoreDifference <= -2);
    }

    private String wonScore() {
        return this.scoreDifference >= 2 ?
                "Win for player1" :
                "Win for player2";
    }

    private boolean isAdvantage() {
        return (this.player1Score > 3 || this.player2Score > 3) &&
                (this.scoreDifference == 1 || this.scoreDifference == -1);
    }

    private String advantagedScore() {
        return this.scoreDifference == 1 ?
                "Advantage player1" :
                "Advantage player2";
    }

    private String otherScore() {
        String player1ScoreName = assignNameTo(this.player1Score);
        String player2ScoreName = assignNameTo(this.player2Score);

        return String.format("%s-%s", player1ScoreName, player2ScoreName);
    }

    private static String assignNameTo(int score) {
        switch (score) {
            case 0:
                return "Love";
            case 1:
                return "Fifteen";
            case 2:
                return "Thirty";
            case 3:
                return "Forty";
            default:
                return "";
        }
    }
}
